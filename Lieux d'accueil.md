# Entreprises
- **Niji**, Identity 1 - EuroRennes, 9A rue de Châtillon, 35000 Rennes - [Carte](https://www.openstreetmap.org/search?whereami=1&query=48.10223%2C-1.67525#map=19/48.10223/-1.67525)
- **CYIM**, 46D Rue Louis Kerautret Botmel, 35200 Rennes, France - [Carte](https://www.openstreetmap.org/node/4225477288#map=19/48.08967/-1.63319)
- **Yes We dev**, 8 Quai Robinot de Saint-Cyr, 35000 Rennes - [Carte](https://www.openstreetmap.org/node/6727809556#map=19/48.10569/-1.71775)


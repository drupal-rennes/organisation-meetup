* Agenda du libre - https://www.agendadulibre.org/
* Meetup Drupal Rennes - https://www.meetup.com/Drupal-Rennes/
* Meetup Drupal France et Francophonie - https://www.meetup.com/drupal-france-francophonie/
* Slack #groupe-rennes - https://drupalfrance.slack.com/archives/CHN0A3B60
* Drupal.org Groupe Rennes - https://groups.drupal.org/rennes-bretagne
* Entreprises faisant du Drupal à Rennes (voir le carnet d'adresse)
* Comptes réseaux sociaux proou perso (Facebook, Twitter, Linkedin, Viadeo x2)
* 